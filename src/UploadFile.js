import { useEffect, useState } from "react";
import { Form, FormGroup, Row, Label, Input, Button } from "reactstrap";
import { getStorage, ref, uploadBytes } from "firebase/storage";

export default function UploadImage() {
  const [file, setFile] = useState(null);

  const onFileChange = (e) => {
    const fileInput = e.target.files[0];
    setFile(fileInput);
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const storage = getStorage();
    const storageRef = ref(storage, "files");
    uploadBytes(storageRef, file).then((snaphot) => {
      console.log("snapshot: ", snaphot);
    });
  };

  return (
    <Row>
      <Form onSubmit={onSubmit}>
        <FormGroup>
          <Label htmlFor="file">File</Label>
          <Input
            type="file"
            id="file"
            placeholder="file"
            name="file"
            onChange={onFileChange}
          />
        </FormGroup>
        <FormGroup className="col">
          <Button type="submit" color="success">
            Upload
          </Button>
        </FormGroup>
      </Form>
    </Row>
  );
}

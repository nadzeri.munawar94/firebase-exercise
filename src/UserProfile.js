import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { Button, Col, Container, Form, Input, Row } from "reactstrap";
import { getDatabase, ref, set, get, child } from "firebase/database";
import { Link } from "react-router-dom";

export default function UserProfile() {
  const [user, setUser] = useState(null);
  const [edit, setEdit] = useState(false);
  const [userId, setUserId] = useState("");
  const navigate = useNavigate();
  const database = getDatabase();

  useEffect(() => {
    checkUser().then((user) => {
      setUserId(user.uid);
      getUserFromDB(user);
    });
  }, []);

  function checkUser() {
    const auth = getAuth();

    return new Promise((resolve, reject) => {
      onAuthStateChanged(auth, (user) => {
        if (user) {
          // User is signed in, see docs for a list of available properties
          // https://firebase.google.com/docs/reference/js/firebase.User
          resolve(user);
        } else {
          navigate("/login");
          reject("User is not logged in");
        }
      });
    });
  }

  function getUserFromDB(user) {
    const dbRef = ref(database);
    get(child(dbRef, `users/${user.uid}`))
      .then((snapshot) => {
        if (snapshot.exists()) {
          setUser(snapshot.val());
        } else {
          createInitialUserDB(user);
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  function createInitialUserDB(user) {
    set(ref(database, "users/" + user.uid), {
      email: user.email,
    }).then((response) => {
      console.log(response);
    });
  }

  const handleInputChange = (e) => {
    const userData = { ...user, [e.target.name]: e.target.value };
    setUser(userData);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    set(ref(database, "users/" + userId), user).then((response) => {
      setEdit(false);
    });
  };

  return (
    <Container>
      <Form onSubmit={handleSubmit}>
        <Row>
          {edit ? "" : <Button onClick={() => setEdit(true)}>Edit</Button>}
        </Row>
        <Row>
          <Col>Email</Col>
          {edit ? (
            <Input
              type="email"
              name="email"
              value={user.email}
              onChange={handleInputChange}
            />
          ) : (
            <Col>{user?.email}</Col>
          )}
        </Row>
        <Row>
          <Col>Phone</Col>
          {edit ? (
            <Input
              type="text"
              name="phone"
              value={user.phone}
              onChange={handleInputChange}
            />
          ) : (
            <Col>{user?.phone}</Col>
          )}
        </Row>
        <Row>
          <Col>Full Name</Col>
          {edit ? (
            <Input
              type="text"
              name="name"
              value={user.name}
              onChange={handleInputChange}
            />
          ) : (
            <Col>{user?.name}</Col>
          )}
        </Row>
        <Row>
          <Col>Description</Col>
          {edit ? (
            <Input
              type="textarea"
              name="description"
              value={user.description}
              onChange={handleInputChange}
            />
          ) : (
            <Col>{user?.description}</Col>
          )}
        </Row>
        <Row>{edit ? <Button type="submit">Submit</Button> : ""}</Row>
      </Form>
    </Container>
  );
}

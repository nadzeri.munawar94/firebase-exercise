import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import Dashboard from "./Dashboard";
import PlayerForm from "./PlayerForm";
import NotFound from "./NotFound";
import "./App.css";
import Lifecycle from "./Lifecycle";
import LifeCycleFunctional from "./LifecycleFunctional";
import Login from "./Login";
import UploadImage from "./UploadFile";
import Database from "./Database";
import Register from "./Register";
import UserProfile from "./UserProfile";

function App() {
  console.log('Add landing page');
  console.log('Feature authentication');
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Dashboard data="data props" />} />
        <Route path="/player/add" element={<PlayerForm />} />
        <Route path="/player/edit/:id" element={<PlayerForm />} />
        <Route path="/lifecycle" element={<Lifecycle />} />
        <Route path="/lifecycle-functional" element={<LifeCycleFunctional />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/upload" element={<UploadImage />} />
        <Route path="/database" element={<Database />} />
        <Route path="/profile" element={<UserProfile />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
}

export default App;

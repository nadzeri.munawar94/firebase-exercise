import { createRef, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Table,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";

export default function Dashboard() {
  const [players, setPlayers] = useState([]);
  const [filters, setFilters] = useState({
    username: "",
    email: "",
    experience: "",
    lvl: "",
  });
  const [modal, setModal] = useState(false);
  const [image, setImage] = useState(null);

  const fetchPlayers = async () => {
    const queryString = Object.keys(filters)
      .map((key) => key + "=" + filters[key])
      .join("&");
    const response = await fetch(
      `http://localhost:4000/api/v1/players?${queryString}`
    );
    const responseJson = await response.json();

    setPlayers(responseJson.data);
  };

  let file = createRef();

  useEffect(() => {
    fetchPlayers();
  }, []);

  const onFilterChange = (e) => {
    const newFilters = { ...filters, [e.target.name]: e.target.value };
    setFilters(newFilters);
  };

  const onFileChange = async(e) => {
    const file = e.target.files[0];
    const formData = new FormData();
    formData.set('image', file, file.name)

    const response = await fetch('http://localhost:4000/api/v1/image', {
      method: 'POST',
      body: file
    });
    const responseJson = await response.json();
    console.log(responseJson);

    // const reader = new FileReader();

    // reader.readAsDataURL(file);

    // reader.addEventListener(
    //   "load",
    //   () => {
    //     setImage(reader.result);
    //   },
    //   false
    // );
  };

  const applyFilters = (e) => {
    e.preventDefault();
    fetchPlayers();
  };

  const toggle = () => {
    setModal(!modal);
  };

  return (
    <Container className="container">
      <Row className="mt-2">
        <Col>
          <Link to="login">
            <Button type="button" color="success">
              Login
            </Button>
          </Link>
        </Col>
      </Row>
      <Row className="mt-2">
        <Col>
          <Link to="register">
            <Button type="button" color="success">
              Register
            </Button>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}

import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
import { initializeApp } from "firebase/app";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

const firebaseConfig = {
  apiKey: "AIzaSyD0e95rQyOG7g5nLk3YA2dM7Efpwphg2_s",
  authDomain: "learn-firebase-761b2.firebaseapp.com",
  projectId: "learn-firebase-761b2",
  storageBucket: "learn-firebase-761b2.appspot.com",
  messagingSenderId: "176696891262",
  appId: "1:176696891262:web:892a3513e5887ac20a852f",
  databaseURL: "https://learn-firebase-761b2-default-rtdb.asia-southeast1.firebasedatabase.app/"
};
initializeApp(firebaseConfig);



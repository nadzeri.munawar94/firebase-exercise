import { useEffect, useState } from "react";
import { Form, FormGroup, Row, Label, Input, Button } from "reactstrap";
import { getDatabase, ref, set, get, child } from "firebase/database";

export default function Database() {
  const [story, setStory] = useState("");
  const [storyDb, setStoryDb] = useState("");
  const database = getDatabase();

  useEffect(() => {
    const dbRef = ref(database);
    get(child(dbRef, `posts`))
      .then((snapshot) => {
        if (snapshot.exists()) {
          setStoryDb(Object.entries(snapshot.val())[0][1].story)
        } else {
          console.log("No data available");
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }, []);

  const onInputChange = (e) => {
    setStory(e.target.value);
  };

  const onSubmit = (e) => {
    e.preventDefault();

    set(ref(database, "posts/" + Date.now()), {
      story: story,
    }).then((response) => {
      console.log(response);
    });
  };

  return (
    <Row>
      <h1>Story from database: {storyDb}</h1>
      <Form onSubmit={onSubmit}>
        <FormGroup>
          <Label htmlFor="story">Write your story here</Label>
          <Input
            type="textarea"
            id="story"
            placeholder="story"
            name="story"
            onChange={onInputChange}
          />
        </FormGroup>
        <FormGroup className="col">
          <Button type="submit" color="success">
            Submit
          </Button>
        </FormGroup>
      </Form>
    </Row>
  );
}

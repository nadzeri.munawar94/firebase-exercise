import { useEffect, useState } from "react";
import {
  Form,
  FormGroup,
  Row,
  Label,
  Input,
  Button,
  Container,
} from "reactstrap";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { useNavigate } from "react-router";

export default function Login() {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });
  const navigate = useNavigate();

  useEffect(() => {});

  const onInputChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    // formData
    const auth = getAuth();
    signInWithEmailAndPassword(auth, formData.email, formData.password)
      .then((userCredential) => {
        navigate('/profile');
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.error(errorCode, errorMessage);
      });
  };

  return (
    <Container>
      <Row>
        <Form onSubmit={onSubmit}>
          <FormGroup>
            <Label htmlFor="email">Email</Label>
            <Input
              type="email"
              id="email"
              placeholder="email"
              name="email"
              onChange={onInputChange}
            />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="password">Password</Label>
            <Input
              type="password"
              id="password"
              placeholder="password"
              name="password"
              onChange={onInputChange}
            />
          </FormGroup>
          <FormGroup className="col">
            <Button type="submit" color="success">
              Login
            </Button>
          </FormGroup>
        </Form>
      </Row>
    </Container>
  );
}

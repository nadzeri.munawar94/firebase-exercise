import { initializeApp } from "firebase/app";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD0e95rQyOG7g5nLk3YA2dM7Efpwphg2_s",
  authDomain: "learn-firebase-761b2.firebaseapp.com",
  projectId: "learn-firebase-761b2",
  storageBucket: "learn-firebase-761b2.appspot.com",
  messagingSenderId: "176696891262",
  appId: "1:176696891262:web:892a3513e5887ac20a852f",
};

// Initialize Firebase
export default initializeApp(firebaseConfig);

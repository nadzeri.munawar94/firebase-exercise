import { Component } from "react";
import { Link } from "react-router-dom";

export default class Lifecycle extends Component {
  state = {
    counter: 0,
  };

  componentDidMount() {
    console.log("component did mount");
    // fetch initial data to BE
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("component did update");
    console.log("prevProps: ", prevProps);
    console.log("prevState: ", prevState);
    console.log("snapshot: ", snapshot);

    // nambah suatu proses
  }

  componentWillUnmount() {
    console.log("component will unmount");
  }

  render() {
    console.log("component is rendering");
    return (
      <>
        <h1>Test lifecycle {this.state.counter}</h1>
        <button
          onClick={() => this.setState({ counter: this.state.counter + 1 })}
        >
          Add Counter
        </button>
        <Link to="/">
          <button type="button">Back</button>
        </Link>
      </>
    );
  }
}

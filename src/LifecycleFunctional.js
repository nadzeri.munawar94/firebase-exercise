import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function LifeCycleFunctional() {
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    console.log("disini mirip lifecycle componentDidMount");
  }, []);

  useEffect(() => {
    console.log("disini gabungan antara lifecycle componentDidMount dgn update di counter");
  }, [counter]);

  return (
    <>
      <h1>Test lifecycle {counter}</h1>
      <button onClick={() => setCounter(counter + 1)}>Add Counter</button>
      <Link to="/">
        <button type="button">Back</button>
      </Link>
    </>
  );
}
